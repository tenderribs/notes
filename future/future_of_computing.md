---
fontfamily: mathpazo
geometry: [a4paper, bindingoffset=0mm, inner=30mm, outer=30mm, top=30mm, bottom=30mm] # See https://ctan.org/pkg/geometry for more options
lang: en-US
color-links: true # See https://ctan.org/pkg/xcolor for colors
linkcolor: black
urlcolor: black
toc: true
title: future of computing
author: Mark Marolf
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead[LO,LE]{\today}
    \fancyhead[RO,RE]{Mark Marolf}
    \linespread{1.1}
---

# Future of Computing

Energy efficiency is going to be the biggest problem in computing.

Some keywords:
- Neuromorphic computing
- https://www.businessinsider.com/ian-pearson-predictions-about-the-world-in-2050-2016-7?r=US&IR=T
- https://rebootingcomputing.ieee.org/archived-articles-and-videos/technology-spotlight/the-future-of-computing-a-conversation-with-john-hennessy
- https://www.nature.com/articles/s41565-020-0738-x