INPUT_FILES=$(wildcard */*.md)
OUTPUT_FILES=$(INPUT_FILES:.md=.html)

all: $(OUTPUT_FILES)

%.html: %.md
	pandoc -s --toc -H ./pandoc.css "--output=$@" --from markdown --to html5 \
		--data-dir=.  --katex=https://cdn.jsdelivr.net/npm/katex@0.13.11/dist/ \
		--variable=pagetitle:$(*F)\ \|\ Mark\ Marolf "$<"
clean:
	rm $(OUTPUT_FILES)